# Learning Server Side Rendered App Architecture

## Stories

1) As a Front End Developer, I can create a very simple client-side react app without using any wizards, so that I understand how react, module bundling and transpiling work together.

2) 

## Approach

- Setup basic project environment with version control.
  - Initialise a git project
  - Configure node environment (nvm config file)
  - Initialise npm
- Create a simple, client side react app.
  - Install essential packages for react.
  - Create a basic html file with conatiner element and which runs the index.js script.
  - Create a index.js script to render the app.
  - Create a basic react App root component.
  - Set up webpack to create a single js file bundle, with config for react.
  - Configure babel to transpile es6 > es5.
  - Use a simple dev server to serve the app locally.

- Create a node.js server.
  - install express packages.
  - create server.js file.
  - add nodemon

- Server side render the app.

## How to Install

Build the app:

```console
$ npm run build
```

## How to Run

I've used the Live Server extension to VS code to serve my app. Right click on `index.html` in the `dist` folder and choose Open with Live Server.

## Learning Notes

- Prefer Functional React Components - smaller and preferred over class based components.
- Node Module Options - Global vs Project vs Dev.
