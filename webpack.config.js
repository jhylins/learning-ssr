const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-env","@babel/preset-react"]
        },
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },             
  plugins: [
    new HtmlWebpackPlugin({template: './src/index.html'})
  ]
};