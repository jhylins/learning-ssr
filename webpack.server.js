const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const path = require('path');

module.exports = {
  entry: './src/server.js',
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'dist'),
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-env","@babel/preset-react"]
        },
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    fallback: {
      "path": false,
      "zlib": false,
      "querystring": false,
      "buffer": false,
      "crypto": false,
      "stream": false,
      "http": false,
      "url": false,
      "util": false,
    },
  },             
  // plugins: [
  //   new HtmlWebpackPlugin({template: './src/index.html'})
  // ]
};