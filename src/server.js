const express = require('express');
const path = require('path');
const ReactDOMServer = require('react-dom/server')
const React = require('react')
const App = require('./App.jsx')
const Html = require('./html')
const fs = require('fs')

const app = express();
const port = 3000;

function handleRender(req, res) {
  // Renders our Hello component into an HTML string
  const html = ReactDOMServer.renderToString(<App />);

  // Load contents of index.html
  fs.readFile('./index.html', 'utf8', function (err, data) {
    if (err) throw err;

    // Inserts the rendered React HTML into our main div
    const document = data.replace(/<div id="root"><\/div>/, `<div id="root">${html}</div>`);

    // Sends the response back to the client
    res.send(document);
  });
}

app.get('*', handleRender);

// app.get('/', (req, res) => {
//   // res.sendFile(path.resolve(__dirname, '../dist/index.html'))
//   console.log('APP>>>>>>',<App/>);
//   //const html = ReactDOMServer.renderToString(<App/>)
//   const appHtml = ReactDOMServer.renderToString((<App />))
//   const html = ReactDOMServer.renderToString((
//     <Html appHtml={appHtml} />
//    ))
//   // res.write(html)
//   res.write(`<!doctype html>${html}`)
//   return res.end()
// })

app.use(express.static('dist'))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
