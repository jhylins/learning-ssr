const Html = ({ appHtml, path, modules, store }) => {
  return (
    <html lang="en-GB">
      <head>
        <meta charSet="utf-8" />
        <title>Hello world</title>
      </head>
      <body>
        <div id="content" dangerouslySetInnerHTML={{ __html: appHtml }} />
        <script src={bundleUrl} charSet="UTF-8" />
      </body>
    </html>
  )
}

export default Html;